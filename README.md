# Tensorflow Pipeline CPU Environment
Base image for Tensorflow Transform and TFX pipelines targeting a CPU environment for Kubeflow 0.6.

Currently uses Tensorflow 1.X due to Tensorflow 2.X incompatibility issues.
Once Tensorflow Transform and TFX support TF 2.X the TF 1.X and 2.X environments should be merged.

Contains miscellanous ML packages as well. Update requirements.txt if dependencies are missing.

## Building 
Image must be built on correct AI-platform node as certain files regarding Data Lake support can not be committed to repo.

### Building instructions
To get a copy of Oracle Instant Client 19.3:
```
wget https://download.oracle.com/otn_software/linux/instantclient/193000/instantclient-basic-linux.x64-19.3.0.0.0dbru.zip
unzip instantclient-basic-linux.x64-19.3.0.0.0dbru.zip
```
TODO! See master branch for an indication of build procedure.

## TODO
* Test in proper Kubeflow environment
    * Ensure AMPB pipeline is runnable
    * Coordinate with team-members that dependencies are not missing
* Investigate proper build procedure
